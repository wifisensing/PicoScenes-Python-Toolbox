# 使用官方的 Python 基础镜像
FROM python:3.10-slim-buster

# 工作目录设置为 /app
WORKDIR /app

# 把当前目录的所有文件添加到工作目录 /app
ADD . /app

# Install software-properties-common to manage repository and distribution specific software properties
RUN apt update && apt-get update && apt-get install -y software-properties-common
# Update the repositories
RUN apt-get update -y

RUN add-apt-repository ppa:ubuntu-toolchain-r/test 

# 使用 pip 安装依赖
# Install software-properties-common to manage repository and distribution specific software properties
RUN apt update && apt-get update -y

# Update the repositories
RUN apt-get update -y

# Install GCC and G++
RUN apt-get install -y gcc-12 g++-12
RUN gcc --version
    
RUN pip install -r requirements.txt
RUN python3 setup.py build_ext --inplace


# 当容器启动时运行 Python 应用
CMD ["python", "test.py"]
